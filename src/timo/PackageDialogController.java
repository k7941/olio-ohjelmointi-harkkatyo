/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Juho
 */
public class PackageDialogController implements Initializable {
    @FXML
    private ComboBox esineetCombobox;
    @FXML
    private ComboBox lahtokaupunkiCombobox;
    @FXML
    private ComboBox lahtoautomaattiCombobox;
    @FXML
    private ComboBox kohdekaupunkiCombobox;
    @FXML
    private ComboBox kohdeautomaattiCombobox;
    @FXML
    private RadioButton luokka1Radiobutton;
    @FXML
    private ToggleGroup luokka;
    @FXML
    private RadioButton luokka2Radiobutton;
    @FXML
    private RadioButton luokka3Radiobutton;
    @FXML
    private TextField nimiTextfield;
    @FXML
    private TextField kokoTextfield;
    @FXML
    private TextField massaTextfield;
    @FXML
    private CheckBox sarkyvaaCheckbox;
    
    private static ArrayList<Item> items = null;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // If it's the first time opening this dialog, create a new items array with 4 random items ready to use
        if(items == null)
        {
            items = new ArrayList<>();

            Item i = new Item(0.002, "3x3x2", "Teepussi", false);
            items.add(i);
            esineetCombobox.getItems().add(i.name);
            i = new Item(0.4, "15x5x3", "Tiili", true);
            items.add(i);
            esineetCombobox.getItems().add(i.name);
            i = new Item(0.3, "35x7x7", "Leipä", false);
            items.add(i);
            esineetCombobox.getItems().add(i.name);
            i = new Item(100, "150x25x25", "Paino", false);
            items.add(i);
            esineetCombobox.getItems().add(i.name);
        }
        else
        {
            //Load the items array into the combobox (there might be custom made items)
            for(Item i : items)
            {
                esineetCombobox.getItems().add(i.name);
            }
        }

        // Fill the other comboboxes
        for (Data usedSmartPost : FXMLDocumentController.smartPost.usedSmartPosts)
        {
            lahtokaupunkiCombobox.getItems().add(usedSmartPost.city + " - " + usedSmartPost.address);
            kohdekaupunkiCombobox.getItems().add(usedSmartPost.city + " - " + usedSmartPost.address);
        }
    }    

    @FXML
    private void infoPressed(ActionEvent event) {
        dialog("1. luokan paketti on kaikista nopein pakettiluokka, jonka vuoksi sitä ei voi lähettää pidemmälle kuin 150 km päähän. Onhan yleisesti tiedossa, että sen pidempää matkaa ei TIMO-mies jaksa pakettia kuljettaa. 1. luokan paketti on myös nopea, koska sen turvallisuudesta ei välitetä niin paljon, jolloin kaikki särkyvät esineet tulevat menemään rikki matkan aikana. \n" +
" \n" +
"\n" +
"2. luokan paketit ovat turvakuljetuksia, jolloin ne kestävät parhaiten kaiken särkyvän tavaran kuljettamisen. Näitä paketteja on mahdollista kuljettaa jopa Lapista Helsinkiin, sillä matkan aikana käytetään useampaa kuin yhtä TIMO-miestä, jolloin turvallinen kuljetus on taattu. Paketissa on kuitenkin huomattava, että jos se on liian suuri, ei särkyvä esine voi olla heilumatta, joten paketin koon on oltava pienempi kuin muilla pakettiluokilla.\n" +
" \n" +
"\n" +
"3. luokan paketti on TIMO-miehen stressinpurkupaketti. Tämä tarkoittaa sitä, että TIMO-miehellä ollessa huono päivä pakettia paiskotaan seinien kautta automaatista toiseen, joten paketin sisällön on oltava myös erityisen kestävää materiaalia. Myös esineen suuri koko ja paino ovat eduksi, jolloin TIMO-mies ei jaksa heittää pakettia seinälle kovin montaa kertaa. Koska paketit päätyvät kohteeseensa aina seinien kautta, on tämä hitain mahdollinen kuljetusmuoto paketille. ",
                "Pakettiluokat");
    }
    
    // Creates a simple dialog with an ok button at the bottom and a label for text.
    private void dialog(String t, String title)
    {
        Text text = new Text(10, 40, t);        
        text.setFont(new Font(15));
        text.setWrappingWidth(600.0);
        
        Stage stage = new Stage();
        
        final BorderPane g = new BorderPane();
        g.setCenter(text);
        Button b = new Button("Ok");
        b.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event)
            {
                ((Stage)g.getScene().getWindow()).close();
            }
        });
        g.setBottom(b);
        
        Scene scene = new Scene(g);

        stage.setTitle(title); 
        stage.setScene(scene);
        
        stage.show(); 
    }
    
    // when the user selects an item from the combobox, find its properties by the name
    private Item findItem(String name) throws Exception
    {
        for(Item i : items)
        {
            if(i.name == name)
                return i;
        }
        
        throw new Exception();
    }

    @FXML
    private void createPackagePressed(ActionEvent event) {
        Item i;
        
        // If all the textfields for create new item are filled, create an item
        if(!massaTextfield.getText().isEmpty() && !nimiTextfield.getText().isEmpty() && !kokoTextfield.getText().isEmpty())
        {
            try
            { // See if the fields were correctly filled
                Integer.parseInt(massaTextfield.getText());
                if(kokoTextfield.getText().split("x").length != 3)
                    throw new Exception();
                for(String s : kokoTextfield.getText().split("x"))
                {
                    Double.parseDouble(s);
                }
            }
            catch(Exception e)
            {
                dialog("Uuden esineen tiedot väärin. Muistitko 2x2x2 formaatin koossa?", "Error"); // Throw dialogs at people for information
                return;
            }
            
            i = new Item( Double.parseDouble( massaTextfield.getText() ),
                    kokoTextfield.getText(),
                    nimiTextfield.getText(),
                    sarkyvaaCheckbox.isSelected());
            
            items.add(i); // Add the new item to the items array and it will be loaded to the combobox in the next opening of this dialog
        }
        else
        { // An already made item was selected
            try
            {
                i = findItem(esineetCombobox.getValue().toString());
            }
            catch(Exception e)
            {
                dialog("Et valinnut esinettä valikosta!!!!!!! :/", "Error");
                return;
            }
        }
        
        // Create the package for the item :^)
        Package p = null;
        if(luokka1Radiobutton.isSelected())
        {
            p = new Class1Package(i, 1);
        }
        if(luokka2Radiobutton.isSelected())
        {
            p = new Class2Package(i, 2);
        }
        if(luokka3Radiobutton.isSelected())
        {
            p = new Class3Package(i, 3);
        }
        
        String[] latlon1 = new String[2];
        String[] latlon2 = new String[2];
        
        try {
            latlon1 = FXMLDocumentController.smartPost.getLatLon( lahtokaupunkiCombobox.getValue().toString().split(" - ")[1] );
            latlon2 = FXMLDocumentController.smartPost.getLatLon( kohdekaupunkiCombobox.getValue().toString().split(" - ")[1] );
            
            p.lahtoautomaatti = lahtoautomaattiCombobox.getValue().toString();
            p.kohdeautomaatti = kohdeautomaattiCombobox.getValue().toString();
        } catch (Exception e) {
            dialog("Muistitko valita lähtö- ja kohdeautomaatit? :()", "Error");
            return;
        }
        
        // Use createPath to get the distance
        Object num = FXMLDocumentController.w.getEngine().executeScript("document.createPath([\"" + latlon1[0] + "\", \"" + latlon1[1] + "\", \"" +
                                                latlon2[0] + "\", \"" + latlon2[1] + "\"], \"red\", " + p.packageClass + ")");
        
        double journeyLength = Double.parseDouble( num.toString() );
        p.setJourneyLength(journeyLength);
        
        if(p.willBeSent == true)
        {
            FXMLDocumentController.storage.addPackage(p);
            ((Stage)lahtoautomaattiCombobox.getScene().getWindow()).close();
        }
        else
        {
            dialog(p.errorMessage, "Error");
        }
    }

    @FXML
    private void cancelPressed(ActionEvent event) {
        ((Stage)lahtoautomaattiCombobox.getScene().getWindow()).close();
    }

    // When the combobox for the city is chosen, add the options for the actual SmartPosts
    @FXML
    private void lahtoKaupunkiEdited(ActionEvent event) {
        lahtoautomaattiCombobox.getItems().clear();
        
        for (Data usedSmartPost : FXMLDocumentController.smartPost.usedSmartPosts)
        {
            if(usedSmartPost.address.equals( lahtokaupunkiCombobox.getValue().toString().split(" - ")[1] ))
                lahtoautomaattiCombobox.getItems().add(usedSmartPost.postoffice);
        }
    }

    @FXML
    private void kohdeKaupunkiEdited(ActionEvent event) {
        kohdeautomaattiCombobox.getItems().clear();
        
        for (Data usedSmartPost : FXMLDocumentController.smartPost.usedSmartPosts)
        {
            if(usedSmartPost.address.equals( kohdekaupunkiCombobox.getValue().toString().split(" - ")[1] ))
                kohdeautomaattiCombobox.getItems().add(usedSmartPost.postoffice);
        }
    }
    
}
