/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import javafx.scene.control.ComboBox;
import org.xml.sax.SAXException;

class Data
{
    public String city;
    public String address;
    public String code;
    public String availability;
    public String lat;
    public String lon;
    public String postoffice;
}

public class SmartPost {
    
    private ArrayList<Data> data;
    public ArrayList<Data> usedSmartPosts; // SmartPosts that are used in the map. Needed for the package dialogs comboboxes

    //Reads the cities from the xml file
    public SmartPost(ComboBox cb) throws ParserConfigurationException, SAXException, IOException {
        data = new ArrayList<>();
        usedSmartPosts = new ArrayList<>();
        
	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new URL("http://smartpost.ee/fi_apt.xml").openStream());
        
        NodeList nList = doc.getElementsByTagName("place");
        for(int i = 0; i < nList.getLength(); i++)
        {
            Data d = new Data();
            Element e = (Element)nList.item(i);
            
            d.city = e.getElementsByTagName("city").item(0).getTextContent();
            d.address = e.getElementsByTagName("address").item(0).getTextContent();
            d.code = e.getElementsByTagName("code").item(0).getTextContent();
            d.availability = e.getElementsByTagName("availability").item(0).getTextContent();
            d.postoffice = e.getElementsByTagName("postoffice").item(0).getTextContent();
            d.lat = ( e.getElementsByTagName("lat").item(0).getTextContent() );
            d.lon = ( e.getElementsByTagName("lng").item(0).getTextContent() );
            
            data.add(d);
            
            cb.getItems().add(d.city + " - " + d.address);
        }
    }
        
    // Main app can ask for the latitude and longitude from just the address string
    public String[] getLatLon(String address)
    {
        for (Data d : data) {
            
            if(d.address.equals(address))
            {
                return new String[] {( d.lat ), ( d.lon )};
            }
        }
        
        return null;
    }
        
    // Returns the full address needed for the javascript function goToLocation in a single string that still needs to be split
    public String getFullAddress(String address)
    {
        for (Data d : data) {
            
            if(d.address.equals(address))
            {
                usedSmartPosts.add(d);
                
                String returnValue;
                returnValue = d.address + ", " + d.code + " " + d.city + "!" + 
                        d.postoffice + " " + d.availability + "!" +
                        "red";
                return returnValue;
            }
        }
        
        return null;
    }
    
}
