/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author Juho
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private ComboBox<?> kaupunkiCombobox;
    @FXML
    private ComboBox varastoCombobox;
    @FXML
    private TextArea logTextarea;
    @FXML
    private WebView webview;
    
    // Used from other files
    public static WebView w;
    public static ComboBox ppC;
    
    public static SmartPost smartPost;
    public static Storage storage;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        TIMO.s.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                try { // Set a custom event handler for closing the app (write the log to the file)
                    exitApplication();
                } catch (IOException ex) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        storage = new Storage();
        w = webview;
        ppC = varastoCombobox;
        
        Scanner s;
        try { // Restore the data from the previous sessions storage file
            s = new Scanner(new File("varasto.txt"));
            while(s.hasNextLine())
            {
                String[] str = s.nextLine().split("!");
                int packageClass = Integer.parseInt( str[4] );
                Item i = new Item(Double.parseDouble(str[0]), str[1], str[2], Boolean.parseBoolean(str[3]));
                
                Package p = null;
                if(packageClass == 1)
                {
                    p = new Class1Package(i, 1);
                }
                if(packageClass == 2)
                {
                    p = new Class2Package(i, 2);
                }
                if(packageClass == 3)
                {
                    p = new Class3Package(i, 3);
                }
                p.lahtoautomaatti = str[6];
                p.kohdeautomaatti = str[7];
                p.setJourneyLength( Double.parseDouble( str[5] ));
                
                storage.addPackage(p);
            }
        } catch (FileNotFoundException ex) {
        }
        
        
        File f = new File("index.html");
        try {
            webview.getEngine().load(f.toURI().toURL().toString());
        } catch (MalformedURLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            smartPost = new SmartPost(kaupunkiCombobox);
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    @FXML
    private void addToMapPressed(ActionEvent event) {
        if(kaupunkiCombobox.getSelectionModel().isEmpty())
            return;
        
        // Use the address to get the full address of the smartpost
        String[] args = smartPost.getFullAddress( kaupunkiCombobox.getValue().toString().split(" - ")[1] ).split("!");
        
        webview.getEngine().executeScript("document.goToLocation(\"" + args[0] + "\", \"" + args[1] + "\", \"" + args[2] + "\")");
    
        
    }

    @FXML
    private void createPackagePress(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("PackageDialog.fxml"));
        
        Scene scene = new Scene(root);
        
        Stage s = new Stage();
        s.setScene(scene);
        s.show();
    }

    @FXML
    private void deletePathsPressed(ActionEvent event) {

        webview.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void updatePackagesPressed(ActionEvent event) {
        varastoCombobox.getItems().clear();
        
        for (Package p : storage.packages) {
            varastoCombobox.getItems().add(p.item.name + " Luokka " + p.packageClass + ": " + p.lahtoautomaatti + " -> " +
                                                 p.kohdeautomaatti );
        }
    }

    @FXML
    private void sendPackagePressed(ActionEvent event)
    {
        if(varastoCombobox.getItems().isEmpty())
            return;
        
        int i = varastoCombobox.getItems().indexOf(varastoCombobox.getValue().toString());
        
        varastoCombobox.getItems().remove( i );
        
        Package p = storage.packages.remove(i);
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        logTextarea.appendText( "(LUOKKA " + p.packageClass + ") " + p.item.name + " Lähetetty " + dateFormat.format(date) +
                                "  " + p.lahtoautomaatti + " -> " + p.kohdeautomaatti + " (" + p.journeyLength + " km)" );
    }
    
    // Custom exit method for saving to log file
    public void exitApplication() throws IOException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        Date date = new Date();
        String fn = "Log " + dateFormat.format(date) + ".txt";
        
        try
        {
            File file = new File(fn);
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(logTextarea.getText());
            for(Package p : storage.packages)
            {
                bw.write("\n(Varasto) " + p.item.name + " " + p.item.size + "cm^3  " + p.item.weight + "kg  Luokka: " +
                         p.packageClass + " " + p.journeyLength + "km " + p.lahtoautomaatti + " -> " + p.kohdeautomaatti + "\n");
            }
            bw.close();

            
            File storageFile = new File("varasto.txt");
            if (!file.exists()) {
                file.createNewFile();
            }

            fw = new FileWriter(storageFile.getAbsoluteFile());
            bw = new BufferedWriter(fw);
            for(Package p : storage.packages)
            {
                bw.write(p.item.weight + "!" + p.item.size + "!" + p.item.name + "!" + p.item.fragile + "!" +
                         p.packageClass + "!" + p.journeyLength + "!" + p.lahtoautomaatti + "!" + p.kohdeautomaatti + "\n");
            }
            bw.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        ((Stage)TIMO.s.getScene().getWindow()).close();
    }
    
}