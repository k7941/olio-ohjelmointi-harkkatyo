/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

/**
 *
 * @author Juho
 */
public class Package {
    
    public Item item;
    public double journeyLength;
    public int packageClass;
    public String lahtoautomaatti;
    public String kohdeautomaatti;
    
    public boolean willBeSent;
    public String errorMessage;

    //This method is overridden in class1package where there is a maximum length
    public void setJourneyLength(double l)
    {
        journeyLength = l;
    }
}

class Class1Package extends Package
{
    public Class1Package(Item i, int c)
    {
        willBeSent = true;
        
        String s[] = i.size.split("x");
        double size = Double.parseDouble(s[0])*Double.parseDouble(s[1])*Double.parseDouble(s[2]);
        
        item = i;
        packageClass = c;
        
        if(i.fragile == true)
        {
            willBeSent = false;
            errorMessage = "Luokan 1 paketit eivät saa sisältää särkyvää tavaraa!!!! >.<";
        }
    }
    
    public void setJourneyLength(double l)
    {
        if(l > 150)
        {
            willBeSent = false;
            errorMessage = "Luokan 1 pakettien matkan pituuden täytyy olla alle 150 km :^)";
        }
        
        journeyLength = l;
    }
}

class Class2Package extends Package
{
    
    public Class2Package(Item i, int c)
    {
        willBeSent = true;
        
        String s[] = i.size.split("x");
        double size = Double.parseDouble(s[0])*Double.parseDouble(s[1])*Double.parseDouble(s[2]);
        
        item = i;
        packageClass = c;
        
        if(size > 10000.0)
        {
            errorMessage = "Luokan 2 pakettien pitäisi olla alle 10000cm^3";
            willBeSent = false;
        }
    }
}

class Class3Package extends Package
{
    
    public Class3Package(Item i, int c)
    {
        String s[] = i.size.split("x");
        double size = Double.parseDouble(s[0])*Double.parseDouble(s[1])*Double.parseDouble(s[2]);
        
        item = i;
        packageClass = c;
        
        if(i.fragile == true)
        {
            willBeSent = false;
            errorMessage = "Luokan 3 paketit eivät saa sisältää särkyvää tavaraa.";
        }
        if(i.weight < 50 && size < 500)
        {
            willBeSent = false;
            errorMessage = "Valitse toki luokka 3 vain vähän suuremmille paketeille (yli 50kg & 500cm^3)";
        }
    }
    
}