/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

/**
 *
 * @author Juho
 */
public class Item {
    public double weight;
    public String size;
    public String name;
    public boolean fragile;

    public Item(double weight, String size, String name, boolean fragile) {
        this.weight = weight;
        this.size = size;
        this.name = name;
        this.fragile = fragile;
    }
    
    
}
